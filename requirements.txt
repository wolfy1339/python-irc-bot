#git+https://github.com/itslukej/zirc.git@v1.2.8#egg=zirc
zirc>=1.2.8
requests>=2.12.4
six>=1.10.0
flask>=0.12.2
iso8601>=0.1.12
